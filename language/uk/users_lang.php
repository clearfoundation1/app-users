<?php

$lang['users_account_locked'] = 'Заблоковано';
$lang['users_account_lock_status'] = 'Стан блокування облікового запису';
$lang['users_account_unlocked'] = 'Розблоковано';
$lang['users_added_user'] = 'Додано користувача';
$lang['users_add_user'] = 'Додати користувача';
$lang['users_app_description'] = 'Додаток «Користувачі» дозволяє адміністратору створювати, видаляти та змінювати користувачів у системі. Інші програми, які підключаються безпосередньо до каталогу користувача, автоматично відображатимуть параметри, доступні для облікового запису користувача.';
$lang['users_app_name'] = 'Користувачі';
$lang['users_app_policies'] = 'Політикі додатка';
$lang['users_apps'] = 'Додатки';
$lang['users_create_group'] = 'Додати групу';
$lang['users_deleted_account'] = 'Видалений обліковий запис';
$lang['users_extension'] = 'Розширення';
$lang['users_first_name'] = 'Ім`я';
$lang['users_first_name_invalid'] = 'Ім’я недійсне.';
$lang['users_full_name_already_exists'] = 'Користувач із таким же повним іменем вже існує.';
$lang['users_full_name'] = 'Повне ім`я';
$lang['users_gid_number'] = 'ID номер групи';
$lang['users_group_id_invalid'] = 'ID групи недійсний.';
$lang['users_groups'] = 'Групи';
$lang['users_home_directory'] = 'Домашній каталог';
$lang['users_home_directory_invalid'] = 'Домашній каталог недійсний.';
$lang['users_last_name_invalid'] = 'Прізвище недійсне.';
$lang['users_last_name'] = 'Прізвище';
$lang['users_lock'] = 'Lock';
$lang['users_login_shell_invalid'] = 'Оболонка входу недійсна.';
$lang['users_login_shell'] = 'Оболонка входу';
$lang['users_name'] = 'Ім`я';
$lang['users_old_password_invalid'] = 'Старий пароль недійсний.';
$lang['users_options'] = 'Параметри';
$lang['users_password_cannot_be_changed_on_this_account'] = 'Пароль не можна змінити на цьому обліковому записі.';
$lang['users_password_in_history'] = 'Пароль знаходиться в історії старих паролів.';
$lang['users_password_invalid'] = 'Пароль недійсний.';
$lang['users_password_is_too_short'] = 'Пароль закороткий.';
$lang['users_password_is_too_young'] = 'Пароль занадто маленький для зміни.';
$lang['users_password_not_changed'] = 'Пароль не змінився.';
$lang['users_password_violates_quality_check'] = 'Пароль не відповідає політиці.';
$lang['users_password_was_reset'] = 'Пароль потрібно скинути.';
$lang['users_plugins'] = 'Плагіни';
$lang['users_reset_password_on_account'] = 'Скинути пароль до облікового запису';
$lang['users_uid_number'] = 'ID номер користувача';
$lang['users_unlock'] = 'Розблокувати';
$lang['users_updated_settings_on_account'] = 'Оновлені налаштування облікового запису';
$lang['users_user_already_exists'] = 'Ім`я користувача вже існує.';
$lang['users_user_details'] = 'Інформація про користувача';
$lang['users_user_id_invalid'] = 'ID користувача недійсний.';
$lang['users_user_information_invalid'] = 'Інформація про користувача недійсна.';
$lang['users_user_manager'] = 'Менеджер користувачів';
$lang['users_username_invalid'] = 'Ім’я користувача недійсне.';
$lang['users_username_is_reserved'] = 'Ім`я користувача зарезервовано.';
$lang['users_user_not_found'] = 'Користувач не знайдений.';
$lang['users_users_and_groups'] = 'Користувачі та групи';
$lang['users_users_storage'] = 'Сховище користувачів';
$lang['users_users'] = 'Користувачі';
$lang['users_user'] = 'Користувач';
